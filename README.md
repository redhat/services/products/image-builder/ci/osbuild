# CI for osbuild

This repository essentially is a mirror of https://github.com/osbuild/osbuild

For each PR on github a _new branch_ `PR-*` is created here to run CI, see trigger here:
https://github.com/osbuild/osbuild/blob/main/.github/workflows/trigger-gitlab.yml
